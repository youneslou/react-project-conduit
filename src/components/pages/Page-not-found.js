import { Fragment } from "react";

const PageNotFound = () => {
  return (
    <Fragment>
      <div class="not-found-page">
        <h3 className="not-found-page__text">Page Not Found</h3>
      </div>
    </Fragment>
  );
};

export default PageNotFound;
