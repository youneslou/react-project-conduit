import { Fragment } from "react";

import ArticleItem from "./item-article";

function ListItmes(props) {
  return (
    <Fragment>
      {props.articles.map((item) => {
        return (
          <ArticleItem
            key={item.slug}
            image={item.author.image}
            username={item.author.username}
            createdAt={item.createdAt}
            updatedAt={item.updatedAt}
            body={item.body}
            favorited={item.favorited}
            tagList={item.tagList}
            favoritesCount={item.favoritesCount}
            title={item.title}
            slug={item.slug}
            description={item.description}
            author={item.author}
            articlesName={props.articlesName}
          />
        );
      })}
    </Fragment>
  );
}
export default ListItmes;
