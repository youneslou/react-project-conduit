import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  articles: [],
  articlesCount: 0,
  isMyFeed: false,
  isMyArticles: true,
  isLoading: true,
  error: null,
};

const articlesSlice = createSlice({
  name: "articles",
  initialState,
  reducers: {
    updateArticles(state, action) {
      state.articles = action.payload;
    },
    updateArticlesCount(state, action) {
      state.articlesCount = action.payload;
    },
    updateIsMyFeed(state, action) {
      state.isMyFeed = action.payload;
    },
    updateIsMyArticles(state, action) {
      state.isMyArticles = action.payload;
    },
    updateIsLoading(state, action) {
      state.isLoading = action.payload;
    },
    updateError(state, action) {
      state.error = action.payload;
    },
    updateArticleFavCount(state, action) {
      let articleToUpdate = state.articles.find(
        (item) => item.slug === action.payload.slug
      );
      articleToUpdate.favoritesCount = action.payload.favoritesCount;
      articleToUpdate.favorited = action.payload.favorited;
    },
    resetArticles(state) {
      state.articles = [];
      state.articlesCount = 0;
      state.isLoading = true;
      state.error = null;
      state.isMyFeed = false;
      state.isMyArticles = true;
    },
  },
});

export const articlesActions = articlesSlice.actions;
export default articlesSlice;
