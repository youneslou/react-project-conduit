import { useSelector, useDispatch } from "react-redux";
import { articlesActions } from "./../../store/articlesSlice";
import fitchFavArticlesWithParames from "./../../store/articlesActions";
import React from "react";
import { followUnfollowUser } from "./../../store/profileActions";
import { useHistory } from "react-router-dom";
import ListPaginationArticles from "./items/list-pagination-articles";

function Profile() {
  const dispatch = useDispatch();
  const profile = useSelector((state) => state.profile.profile);
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);
  const user = useSelector((state) => state.auth.user);
  const articles = useSelector((state) => state.articles.articles);
  const isLoading = useSelector((state) => state.articles.isLoading);
  const isProfileLoading = useSelector((state) => state.profile.isLoading);
  const isMyArticles = useSelector((state) => state.articles.isMyArticles);
  const articlesCount = useSelector((state) => state.articles.articlesCount);
  const nav = useHistory();

  const favoritedArticleHandler = (e) => {
    e.preventDefault();
    dispatch(articlesActions.updateIsMyArticles(false));
    dispatch(
      fitchFavArticlesWithParames(
        `?favorited=${profile.username}&limit=5&offset=0`
      )
    );
  };
  const myArticleHandler = (e) => {
    e.preventDefault();
    dispatch(articlesActions.updateIsMyArticles(true));
    dispatch(
      fitchFavArticlesWithParames(
        `?author=${profile.username}&limit=5&offset=0`
      )
    );
  };
  const followUnfollowHandler = (e) => {
    if (profile.following) {
      dispatch(followUnfollowUser("DELETE", profile.username));
    } else {
      dispatch(followUnfollowUser("POST", profile.username));
    }
  };

  const editProfilHandler = (e) => {
    nav.push("/settings");
  };
  return (
    <React.Fragment>
      {!isProfileLoading && (
        <div className="profile-page">
          <div className="user-info">
            <div className="container">
              <div className="row">
                <div className="col-xs-12 col-md-10 offset-md-1">
                  <img
                    src={`${profile.image}`}
                    className="user-img"
                    alt="Profile img"
                  />
                  <h4>{profile.username}</h4>
                  <p>{profile.bio}</p>
                  {profile.username === user.username ? (
                    <button
                      className="btn btn-sm btn-outline-secondary action-btn"
                      onClick={editProfilHandler}
                    >
                      <i className="ion-plus-round"></i>
                      &nbsp; Edite Profile
                    </button>
                  ) : (
                    <button
                      className="btn btn-sm btn-outline-secondary action-btn"
                      onClick={followUnfollowHandler}
                    >
                      <i className="ion-plus-round"></i>
                      &nbsp;{" "}
                      {`${profile.following ? "UnFollow" : "Follow"} ${
                        profile.username
                      }`}
                    </button>
                  )}
                </div>
              </div>
            </div>
          </div>

          <div className="container">
            <div className="row">
              <div className="col-xs-12 col-md-10 offset-md-1 articles">
                <div className="articles-toggle">
                  <ul className="nav nav-pills outline-active">
                    <li className="nav-item">
                      <a
                        className={`nav-link ${
                          isMyArticles ? "active" : ""
                        } nav-link-cost ${isLoading && "disabled"}`}
                        href=""
                        onClick={myArticleHandler}
                      >
                        My Articles
                      </a>
                    </li>
                    <li className="nav-item">
                      <a
                        className={`nav-link ${
                          isMyArticles ? "" : "active"
                        } nav-link-cost ${isLoading && "disabled"}`}
                        href=""
                        onClick={favoritedArticleHandler}
                      >
                        Favorited Articles
                      </a>
                    </li>
                  </ul>
                </div>
                <ListPaginationArticles
                  articles={articles}
                  isLoading={isLoading}
                  articlesName={isMyArticles ? "MyArticles" : "MyFavArticles"}
                  articlesCount={articlesCount}
                  username={profile.username}
                />
              </div>
            </div>
          </div>
        </div>
      )}
    </React.Fragment>
  );
}
export default Profile;
