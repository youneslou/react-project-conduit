import { Link, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import fitchArticle from "./../../../store/articleActions";
import fitchComments from "./../../../store/commentsActions";
import TagList from "./list-tags";
import fitchProfile from "./../../../store/profileActions";
import fitchFavArticlesWithParames from "./../../../store/articlesActions";
import useHttp from "./../../../hooks/use-http";
import { articlesActions } from "./../../../store/articlesSlice";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-solid-svg-icons";

function ArticleItem(props) {
  const dispatch = useDispatch();
  const nav = useHistory();
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);
  const { sendRequest, isLoading, error } = useHttp();

  const showArticleHandler = (e) => {
    e.preventDefault();
    dispatch(fitchArticle(`/${props.slug}`));
    dispatch(fitchComments(props.slug));
    dispatch(fitchProfile(props.username));
    nav.push(`/article/${props.slug}`);
  };

  const favoriteCountHandler = (e) => {
    if (isAuthenticated) {
      let method = "";

      if (props.favorited) {
        method = "DELETE";
      } else {
        method = "POST";
      }

      const applyData = (data) => {
        console.log(props.articlesName);
        dispatch(articlesActions.updateArticleFavCount(data.article));
      };
      sendRequest(
        {
          url: `https://conduit.productionready.io/api/articles/${props.slug}/favorite`,
          method: method,
          authorization: `Token ${localStorage.getItem("jwtToken")}`,
        },
        applyData
      );
    } else {
      nav.push("/login");
    }
  };

  const showProfileHandler = (e) => {
    e.preventDefault();
    dispatch(fitchProfile(props.username));
    dispatch(
      fitchFavArticlesWithParames(`?author=${props.username}&limit=5&offset=0`)
    );
    dispatch(articlesActions.updateIsMyArticles(true));
    nav.push(`/profile/${props.username}`);
  };

  return (
    <div className="article-preview">
      <div className="article-meta">
        <a onClick={showProfileHandler} href="">
          <img src={props.image} />
        </a>
        <div className="info">
          <a className="author" onClick={showProfileHandler} href="">
            {props.username}
          </a>
          <span className="date">
            {Intl.DateTimeFormat("en-US").format(new Date(props.createdAt))}
          </span>
        </div>
        <button
          className={`btn btn-outline-primary btn-sm pull-xs-right ${
            isAuthenticated && props.favorited && "active"
          }`}
          onClick={favoriteCountHandler}
        >
          <FontAwesomeIcon icon={faHeart} /> {props.favoritesCount}
          {/* <i className="ion-heart"></i> {props.favoritesCount} */}
        </button>
      </div>
      <Link
        className="preview-link"
        to={`/article/${props.slug}`}
        onClick={showArticleHandler}
      >
        <h1>{props.title}</h1>
        <p>{props.description}</p>
        <span>Read more...</span>
        <TagList
          isArticleItem={true}
          tagList={props.tagList}
          isTagsLoading={false}
        />
      </Link>
    </div>
  );
}
export default ArticleItem;
