import Form from "./form/log-reg-form";
import { useLocation } from "react-router-dom";
const formParames = (isLoading, error) => {};

function LogReg(props) {
  const pathname = useLocation().pathname;

  return (
    <div className="auth-page">
      <div className="container page">
        <div className="row">
          <Form pathname={pathname} formParames={formParames} />
        </div>
      </div>
    </div>
  );
}

export default LogReg;
