import Input from "./../../ui/input";
import Button from "./../../ui/Button";
import { useLocation, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { fetchUser } from "../../../store/authActions";
import useHttp from "./../../../hooks/use-http";
import useInput from "./../../../hooks/use-input";
import { tagListActions } from "./../../../store/tagsSlice";

const Form = (props) => {
  const pathname = useLocation().pathname;
  const dispatch = useDispatch();
  const nav = useHistory();
  const { sendRequest, isLoading, error } = useHttp();
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);

  props.formParames(isLoading, error);

  if (isAuthenticated) {
    nav.push("/");
  }

  const {
    value: enteredName,
    isValid: nameIsValid,
    hasError: nameHasError,
    setValue: setName,
    valueChangeHandler: nameChangeHandler,
    valueBlurHandler: nameBlurHandler,
    reset: resetName,
  } = useInput((value) => value !== "");
  const {
    value: enteredEmail,
    isValid: emailIsValid,
    hasError: emailHasError,
    setValue: setEmail,
    valueChangeHandler: emailChangeHandler,
    valueBlurHandler: emailBlurHandler,
    reset: resetEmail,
  } = useInput((value) => value !== "");
  const {
    value: enteredPassword,
    isValid: passwordIsValid,
    hasError: passwordHasError,
    setValue: setPassword,
    valueChangeHandler: passwordChangeHandler,
    valueBlurHandler: passwordBlurHandler,
    reset: resetPassword,
  } = useInput((value) => value !== "");

  let formIsValid = false;

  if (pathname === "/login") {
    formIsValid = emailIsValid && passwordIsValid;
  } else if (pathname === "/register") {
    formIsValid = nameIsValid && emailIsValid && passwordIsValid;
  }

  const formHandler = (e) => {
    e.preventDefault();

    if (pathname === "/register") {
      const applyData = (data) => {
        console.log(data);
        nav.push("/login");
      };
      sendRequest(
        {
          url: "https://conduit.productionready.io/api/users",
          method: "POST",
          body: {
            user: {
              username: enteredName,
              email: enteredEmail,
              password: enteredPassword,
            },
          },
        },
        applyData
      );
    } else if (pathname === "/login") {
      dispatch(fetchUser(enteredEmail, enteredPassword));
      dispatch(tagListActions.resetTag());
    }
  };

  const loginHandler = (e) => {
    nav.push("/login");
  };
  const registerHandler = (e) => {
    nav.push("/register");
  };

  return (
    <div className="col-md-6 offset-md-3 col-xs-12">
      <h1 className="text-xs-center">Sign up</h1>
      <p className="text-xs-center">
        {pathname === "/login" && (
          <a href="" onClick={loginHandler}>
            Have an account?
          </a>
        )}
        {pathname === "/register" && (
          <a href="" onClick={registerHandler}>
            Do not have an account?
          </a>
        )}
      </p>

      <ul className="error-messages">
        <li>That email is already taken</li>
      </ul>
      <form onSubmit={formHandler}>
        {pathname === "/register" && (
          <Input
            type={"text"}
            placeholder={"Your Name"}
            value={enteredName}
            onChange={nameChangeHandler}
            onBlur={nameBlurHandler}
          />
        )}
        <Input
          type={"text"}
          placeholder={"Email"}
          value={enteredEmail}
          onChange={emailChangeHandler}
          onBlur={emailBlurHandler}
        />
        <Input
          type={"text"}
          placeholder={"Password"}
          value={enteredPassword}
          onChange={passwordChangeHandler}
          onBlur={passwordBlurHandler}
        />

        <Button
          disabled={!formIsValid}
          content={"Sign up"}
          type={"submit"}
        ></Button>
      </form>
    </div>
  );
};
export default Form;
