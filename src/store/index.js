import { configureStore } from "@reduxjs/toolkit";
import articlesSlice from "./articlesSlice";
import authSlice from "./authSlice";
import profileSlice from "./profileSlice";
import updateArticle from "./articleSlice";
import tagList from "./tagsSlice";
import updatecomments from "./commentsSlice";

const store = configureStore({
  reducer: {
    auth: authSlice.reducer,
    articles: articlesSlice.reducer,
    article: updateArticle.reducer,
    profile: profileSlice.reducer,
    tag: tagList.reducer,
    comments: updatecomments.reducer,
  },
});

export default store;
