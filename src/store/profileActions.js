import { profileActions } from "./profileSlice";
import axios from "axios";

const fitchProfile = (username) => {
  return async (dispatch) => {
    const sendRequest = async () => {
      dispatch(profileActions.updateError(null));
      dispatch(profileActions.updateIsLoading(true));

      const response = await fetch(
        `https://conduit.productionready.io/api/profiles/${username}`,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            authorization: `Token ${localStorage.getItem("jwtToken")}`,
          },
        }
      );
      console.log(response);
      if (!response.ok) {
        throw new Error("echec");
      }
      const data = await response.json();
      return data;
    };
    try {
      const data = await sendRequest();
      console.log(data);
      dispatch(profileActions.addProfile(data.profile));
      dispatch(profileActions.updateIsLoading(false));
    } catch (error) {
      dispatch(profileActions.updateError(error.message || "error"));
    }
  };
};
export const followUnfollowUser = (methodeParam, username) => {
  return async (dispatch) => {
    const sendRequest = async () => {
      dispatch(profileActions.updateFollowError(null));
      dispatch(profileActions.updateIsFollowLoading(true));

      const response = await fetch(
        `https://conduit.productionready.io/api/profiles/${username}/follow`,
        {
          method: methodeParam,
          headers: {
            Accept: "application/json",
            authorization: `Token ${localStorage.getItem("jwtToken")}`,
          },
        }
      );
      if (!response.ok) {
        throw new Error("echec");
      }
      const data = await response.json();
      return data;
    };
    try {
      const data = await sendRequest();
      dispatch(profileActions.addProfile(data.profile));
      dispatch(profileActions.updateIsFollowLoading(false));
    } catch (error) {
      dispatch(profileActions.updateFollowError(error.message || "error"));
    }
  };
};
export default fitchProfile;
