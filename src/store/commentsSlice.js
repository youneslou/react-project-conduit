import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  comments: [],
  isLoading: true,
  isCommentLoading: true,
  error: null,
  commentError: null,
};

const updatecomments = createSlice({
  name: "comment",
  initialState,
  reducers: {
    updateComments(state, action) {
      state.comments = action.payload;
    },
    updateIsLoading(state, action) {
      state.isLoading = action.payload;
    },
    updateIsCommentLoading(state, action) {
      state.isCommentLoading = action.payload;
    },
    addComment(state, action) {
      state.comments.push(action.payload);
    },
    removeComment(state, action) {
      const oldStrate = state.comments;
      state.comments = oldStrate.filter((item) => item.id !== action.payload);
    },
    updateError(state, action) {
      state.error = action.payload;
    },
    updateCommentError(state, action) {
      state.commentError = action.payload;
    },
    resetComments(state) {
      state.comments = [];
      state.isLoading = true;
      state.error = null;
      state.isCommentLoading = true;
      state.commentError = null;
    },
  },
});

export const updatecommentsActions = updatecomments.actions;
export default updatecomments;
