import { useState, useCallback } from "react";

const useHttp = () => {
  const [isLoading, setIsLoding] = useState(true);
  const [error, setError] = useState(null);

  const sendRequest = useCallback(async (config, applyData) => {
    setIsLoding(true);
    setError(null);
    try {
      const response = await fetch(config.url, {
        method: config.method ? config.method : "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          authorization: config.authorization ? config.authorization : null,
        },
        body: config.body ? JSON.stringify(config.body) : null,
      });
      if (!response.ok) {
        throw new Error("echec");
      }
      const data = await response.json();

      await applyData(data);
      setIsLoding(false);
    } catch (error) {
      setError(error.message || "error");
    }

    setIsLoding(false);
  }, []);
  return { sendRequest, isLoading, error };
};
export default useHttp;
