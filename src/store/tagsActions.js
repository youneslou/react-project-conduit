import { tagListActions } from "./tagsSlice";

const fitchTags = () => {
  return async (dispatch) => {
    const sendRequest = async () => {
      dispatch(tagListActions.updateIsLoading(true));

      const response = await fetch(
        "https://conduit.productionready.io/api/tags"
      );
      if (!response.ok) {
        throw new Error("echec");
      }
      const data = await response.json();
      return data;
    };
    try {
      const data = await sendRequest();

      dispatch(tagListActions.updateTagList(data.tags));
      dispatch(tagListActions.updateIsLoading(false));
    } catch (error) {
      dispatch(tagListActions.updateError(error.message || "error"));
    }
  };
};

export default fitchTags;
