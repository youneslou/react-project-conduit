import { updatecommentsActions } from "./commentsSlice";

const fitchComments = (slug) => {
  return async (dispatch) => {
    const sendRequest = async () => {
      dispatch(updatecommentsActions.updateIsLoading(true));
      dispatch(updatecommentsActions.updateError(null));

      const response = await fetch(
        `https://conduit.productionready.io/api/articles/${slug}/comments`
      );
      if (!response.ok) {
        throw new Error("echec");
      }
      const data = await response.json();
      return data;
    };
    try {
      const data = await sendRequest();

      dispatch(updatecommentsActions.updateComments(data.comments));
      dispatch(updatecommentsActions.updateIsLoading(false));
    } catch (error) {
      dispatch(updatecommentsActions.updateError(error.message || "error"));
    }
  };
};

export const addComment = (slug, commentBody) => {
  return async (dispatch) => {
    const sendRequest = async () => {
      dispatch(updatecommentsActions.updateIsCommentLoading(true));
      dispatch(updatecommentsActions.updateCommentError(null));

      const response = await fetch(
        `https://conduit.productionready.io/api/articles/${slug}/comments`,
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            authorization: `Token ${localStorage.getItem("jwtToken")}`,
          },
          body: JSON.stringify({ comment: commentBody }),
        }
      );
      if (!response.ok) {
        throw new Error("echec");
      }
      const data = await response.json();
      return data;
    };
    try {
      const data = await sendRequest();
      console.log(data.comment);

      dispatch(updatecommentsActions.addComment(data.comment));
      dispatch(updatecommentsActions.updateIsCommentLoading(false));
    } catch (error) {
      dispatch(
        updatecommentsActions.updateCommentError(error.message || "error")
      );
    }
  };
};

export const removeComment = (slug, idComment) => {
  return async (dispatch) => {
    const sendRequest = async () => {
      dispatch(updatecommentsActions.updateIsCommentLoading(true));
      dispatch(updatecommentsActions.updateCommentError(null));

      const response = await fetch(
        `https://conduit.productionready.io/api/articles/${slug}/comments/${idComment}`,
        {
          method: "DELETE",
          headers: {
            Accept: "application/json",
            authorization: `Token ${localStorage.getItem("jwtToken")}`,
          },
        }
      );
      if (!response.ok) {
        throw new Error("echec");
      }
      const data = await response.json();
      return data;
    };
    try {
      const data = await sendRequest();
      dispatch(updatecommentsActions.updateIsCommentLoading(false));
    } catch (error) {
      dispatch(
        updatecommentsActions.updateCommentError(error.message || "error")
      );
    }
  };
};

export default fitchComments;
