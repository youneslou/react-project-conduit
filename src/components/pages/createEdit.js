import useHttp from "./../../hooks/use-http";
import { useHistory, useRouteMatch } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { articleActions } from "./../../store/articleSlice";
import fitchComments from "./../../store/commentsActions";
import useInput from "./../../hooks/use-input";

function CreateEdit() {
  const { sendRequest, isLoading, error } = useHttp();
  const dispatch = useDispatch();
  const nav = useHistory();
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);
  const article = useSelector((state) => state.article.article);
  const locationMatch = useRouteMatch();

  console.log(article);

  const isEdit = locationMatch.params.edit === "true";

  const {
    value: enteredTitle,
    isValid: titleIsValid,
    hasError: titleHasError,
    setValue: setTitle,
    valueChangeHandler: titleChangeHandler,
    valueBlurHandler: titleBlurHandler,
    reset: resetTitle,
  } = useInput((value) => value !== "");
  const {
    value: enteredDescription,
    isValid: descriptionIsValid,
    hasError: descriptionHasError,
    setValue: setDescription,
    valueChangeHandler: descriptionChangeHandler,
    valueBlurHandler: descriptionBlurHandler,
    reset: resetDescription,
  } = useInput((value) => value !== "");
  const {
    value: enteredBody,
    isValid: bodyIsValid,
    hasError: dodyHasError,
    setValue: setBody,
    valueChangeHandler: bodyChangeHandler,
    valueBlurHandler: bodyBlurHandler,
    reset: resetBody,
  } = useInput((value) => value !== "");
  const {
    value: enteredTagList,
    isValid: tagListIsValid,
    hasError: tagListHasError,
    setValue: setTageList,
    valueChangeHandler: tagListChangeHandler,
    valueBlurHandler: tagListBlurHandler,
    reset: resetTagList,
  } = useInput((value) => true);

  let formIsValid = false;
  if (titleIsValid && descriptionIsValid && bodyIsValid) {
    formIsValid = true;
  }

  useEffect(() => {
    if (isAuthenticated && isEdit) {
      setTitle(article.title);
      setDescription(article.description);
      setBody(article.body);
      setTageList(article.tagList);
    } else if (isAuthenticated && !isEdit) {
      console.log("********init edit false*******");

      resetTitle();
      resetDescription();
      resetBody();
      resetTagList();
    }
  }, [isAuthenticated, isEdit, article]);

  const formHandler = (e) => {
    e.preventDefault();

    const tagList = `${enteredTagList}`.trim().split(",");

    const applyData = (data) => {
      dispatch(articleActions.updateArticle(data.article));
      dispatch(fitchComments(data.article.slug));
      nav.push(`/article/${data.article.slug}`);
    };
    sendRequest(
      {
        url: "https://conduit.productionready.io/api/articles",
        method: "POST",
        body: {
          article: {
            title: enteredTitle,
            description: enteredDescription,
            body: enteredBody,
            tagList,
          },
        },
        authorization: `Token ${localStorage.getItem("jwtToken")}`,
      },
      applyData
    );
  };

  return (
    <div className="editor-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-10 offset-md-1 col-xs-12">
            <form onSubmit={formHandler}>
              <fieldset>
                <fieldset className="form-group">
                  <input
                    type="text"
                    className="form-control form-control-lg"
                    placeholder="Article Title"
                    value={enteredTitle}
                    onChange={titleChangeHandler}
                    onBlur={titleBlurHandler}
                  />
                </fieldset>
                <fieldset className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="What's this article about?"
                    value={enteredDescription}
                    onChange={descriptionChangeHandler}
                    onBlur={descriptionBlurHandler}
                  />
                </fieldset>
                <fieldset className="form-group">
                  <textarea
                    className="form-control"
                    rows="8"
                    placeholder="Write your article (in markdown)"
                    value={enteredBody}
                    onChange={bodyChangeHandler}
                    onBlur={bodyBlurHandler}
                  ></textarea>
                </fieldset>
                <fieldset className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Enter tags"
                    value={enteredTagList}
                    onChange={tagListChangeHandler}
                    onBlur={tagListBlurHandler}
                  />
                  <div className="tag-list"></div>
                </fieldset>
                <button
                  className="btn btn-lg pull-xs-right btn-primary"
                  type="submit"
                  disabled={!formIsValid}
                >
                  Publish Article
                </button>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateEdit;
