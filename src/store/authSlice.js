import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  user: {},
  isAuthenticated: false,
  isLoading: false,
  isRander: false,
  error: null,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    updateUser(state, action) {
      state.user = action.payload;
    },
    login(state, action) {
      state.user = action.payload;
      state.isAuthenticated = true;
      state.isRander = true;
    },
    updateIsLoading(state, action) {
      state.isLoading = action.payload;
    },
    updateIsRander(state, action) {
      state.isRander = action.payload;
    },
    updateError(state, action) {
      state.error = action.payload;
    },
    logout(state) {
      state.isAuthenticated = false;
      state.isMyFeed = false;
      state.user = {};
      state.isRander = true;
      state.error = null;
    },
  },
});

export const authActions = authSlice.actions;
export default authSlice;
