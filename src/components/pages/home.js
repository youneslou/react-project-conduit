import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import TagList from "./items/list-tags";
import fitchTags from "./../../store/tagsActions";
import { tagListActions } from "./../../store/tagsSlice";
import { articlesActions } from "./../../store/articlesSlice";
import ListPaginationArticles from "./items/list-pagination-articles";

function Home(props) {
  const [isMyFeed, setIsMyFeed] = useState(true);
  const dispatch = useDispatch();
  const isAuthenticated = useSelector((store) => store.auth.isAuthenticated);
  const isLoading = useSelector((store) => store.articles.isLoading);

  const tagList = useSelector((store) => store.tag.tagList);
  const isTagsLoading = useSelector((store) => store.tag.isLoading);
  const selectedTag = useSelector((store) => store.tag.selectedTag);
  const isTag = useSelector((store) => store.tag.isTag);

  useEffect(() => {
    dispatch(fitchTags());
  }, [dispatch]);

  useEffect(() => {
    if (!isAuthenticated) setIsMyFeed(false);
  }, [isAuthenticated, dispatch]);

  const showFeedArticlesHandler = (e) => {
    e.preventDefault();
    setIsMyFeed(true);
    dispatch(articlesActions.resetArticles());
    dispatch(tagListActions.updateIsTag(false));
  };

  const showArticlesHandler = (e) => {
    e.preventDefault();
    setIsMyFeed(false);
    dispatch(articlesActions.resetArticles());
    dispatch(tagListActions.updateIsTag(false));
  };

  const showTagArticlesHandler = (e) => {
    e.preventDefault();
    setIsMyFeed(false);
    dispatch(tagListActions.updateIsTag(true));
    dispatch(articlesActions.resetArticles());
  };

  return (
    <div className="home-page">
      <div className="banner">
        <div className="container">
          <h1 className="logo-font">conduit</h1>
          <p>A place to share your knowledge.</p>
        </div>
      </div>

      <div className="container page">
        <div className="row">
          <div className="col-md-9">
            <div className="feed-toggle">
              <ul className="nav nav-pills outline-active">
                {isAuthenticated && (
                  <li className="nav-item">
                    <a
                      className={`nav-link ${
                        isMyFeed && !isTag ? "active" : ""
                      } ${isLoading && "disabled"}`}
                      href=""
                      onClick={showFeedArticlesHandler}
                    >
                      Your Feed
                    </a>
                  </li>
                )}
                <li className="nav-item">
                  <a
                    className={`nav-link ${
                      !isMyFeed && !isTag ? "active" : ""
                    } ${isLoading && "disabled"}`}
                    href=""
                    onClick={showArticlesHandler}
                    disabled={isLoading}
                  >
                    Global Feed
                  </a>
                </li>
                {isTag && (
                  <li className="nav-item">
                    <a
                      className={`nav-link ${isTag ? "active" : ""} ${
                        isLoading && "disabled"
                      }`}
                      href=""
                      onClick={showTagArticlesHandler}
                    >
                      {`#${selectedTag}`}
                    </a>
                  </li>
                )}
              </ul>
            </div>

            <ListPaginationArticles
              articlesName={isMyFeed ? "MyFeed" : "global"}
            />
          </div>
          {/* <div className="col-md-9">ffffffffffffffffff</div> */}

          <div className="col-md-3">
            <div className="sidebar">
              <p>Popular Tags</p>
              <TagList
                isArticleItem={false}
                tagList={tagList}
                isTagsLoading={isTagsLoading}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
