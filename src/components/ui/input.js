const Input = (props) => {
  return (
    <div className="form-group">
      <input
        className="form-control form-control-lg"
        type={props.type}
        placeholder={props.placeholder}
        onChange={props.onChange}
        onBlur={props.onBlur}
        value={props.value}
      />
    </div>
  );
};

export default Input;
