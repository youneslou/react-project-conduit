import { useState } from "react";

const useInput = (validateValue) => {
  const [enteredValue, setEnteredValue] = useState("");
  const [isTouched, setIsTouched] = useState(false);

  const valueIsValid = validateValue(enteredValue);
  const hasError = !valueIsValid && isTouched;

  const valueChangeHandler = (e) => {
    setEnteredValue(e.target.value);
  };

  const valueBlurHandler = (e) => {
    setIsTouched(true);
  };

  const reset = () => {
    setEnteredValue("");
    setIsTouched(false);
  };
  const setValue = (value) => {
    setEnteredValue(value);
  };

  return {
    value: enteredValue,
    isValid: valueIsValid,
    hasError,
    setValue,
    valueChangeHandler,
    valueBlurHandler,
    reset,
  };
};

export default useInput;
