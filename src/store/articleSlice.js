import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  article: {},
  isLodaing: true,
  error: null,
};

const updateArticle = createSlice({
  name: "article",
  initialState,
  reducers: {
    updateArticle(state, action) {
      state.article = action.payload;
    },
    updateIsLoading(state, action) {
      state.isLoading = action.payload;
    },
    updateError(state, action) {
      state.error = action.payload;
    },
    resetArticle(state, action) {
      state.article = {};
      state.isLodaing = true;
      state.error = null;
    },
  },
});

export const articleActions = updateArticle.actions;
export default updateArticle;
