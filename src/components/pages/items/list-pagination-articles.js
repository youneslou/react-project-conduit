import { useState, useEffect, Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import ListItmes from "./list-articles";
import { fitchMyFeed } from "./../../../store/articlesActions";
import fitchArticles from "./../../../store/articlesActions";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight, faArrowLeft } from "@fortawesome/free-solid-svg-icons";

const ListPaginationArticles = (props) => {
  const dispatch = useDispatch();
  const selectedTag = useSelector((store) => store.tag.selectedTag);
  const articles = useSelector((state) => state.articles.articles);
  const isLoading = useSelector((state) => state.articles.isLoading);
  const articlesCount = useSelector((state) => state.articles.articlesCount);

  const [pages] = useState(Math.round(!isLoading ? articlesCount : 10 / 10));
  const [currentPage, setCurrentPage] = useState(0);

  const { articlesName, username } = props;

  useEffect(() => {
    if (articlesName !== "") setCurrentPage(0);
  }, [articlesName]);

  useEffect(() => {
    console.log(articlesName);
    switch (articlesName) {
      case "MyFeed": {
        dispatch(fitchMyFeed(`?limit=10&offset=${currentPage * 10}`));
        break;
      }
      case "global": {
        dispatch(fitchArticles(`?limit=10&offset=${currentPage * 10}`));
        break;
      }
      case "MyArticles": {
        dispatch(
          fitchArticles(
            `?author=${username}&limit=10&offset=${currentPage * 10}`
          )
        );
        break;
      }
      case "MyFavArticles": {
        dispatch(
          fitchArticles(
            `?favorited=${username}&limit=10&offset=${currentPage * 10}`
          )
        );
        break;
      }

      default: {
        dispatch(
          fitchArticles(
            `?tag=${selectedTag}&limit=10&offset=${currentPage * 10}`
          )
        );
        break;
      }
    }
  }, [username, articlesName, currentPage, selectedTag, dispatch]);

  function goToNextPage() {
    setCurrentPage((page) => page + 1);
  }

  function goToPreviousPage() {
    setCurrentPage((page) => page - 1);
  }

  function changePage(event) {
    const pageNumber = Number(event.target.textContent);
    setCurrentPage(pageNumber);
  }

  const getPaginationGroup = () => {
    let start = Math.floor(currentPage / 5) * 5;
    return new Array(5).fill().map((_, idx) => start + idx);
  };

  return (
    <Fragment>
      {isLoading ? (
        <div className="article-preview">Loading Articles...</div>
      ) : (
        <ListItmes articlesName={props.articlesName} articles={articles} />
      )}
      {articlesCount > 10 && (
        <div className="pagination">
          <button
            onClick={goToPreviousPage}
            className={`prev ${currentPage === 0 ? "disabled" : ""}`}
          >
            <FontAwesomeIcon icon={faArrowLeft} />
          </button>

          {getPaginationGroup().map((item, index) => (
            <button
              key={index}
              onClick={changePage}
              className={`paginationItem ${
                currentPage === item ? "active" : null
              }`}
            >
              <span>{item}</span>
            </button>
          ))}

          <button
            onClick={goToNextPage}
            className={`next ${currentPage === pages ? "disabled" : ""}`}
          >
            <FontAwesomeIcon icon={faArrowRight} />
          </button>
        </div>
      )}
    </Fragment>
  );
};

export default ListPaginationArticles;
