import { Fragment } from "react";
import { useDispatch } from "react-redux";
import fitchArticles from "./../../../store/articlesActions";
import { tagListActions } from "./../../../store/tagsSlice";
import { articlesActions } from "./../../../store/articlesSlice";

const TagItem = (props) => {
  const dispatch = useDispatch();
  let tag = "";

  const showTagHandler = (e) => {
    e.preventDefault();
    dispatch(articlesActions.resetArticles());
    dispatch(fitchArticles(`?tag=${props.tagName}&limit=10&offset=0`));
    dispatch(tagListActions.updateSelectedTag(props.tagName));
    dispatch(tagListActions.updateIsTag(true));
  };

  if (props.isArticleItem) {
    tag = (
      <a
        href=""
        className="tag-default tag-pill tag-outline ng-binding ng-scope"
      >
        {props.tagName}
      </a>
    );
  } else {
    tag = (
      <a href="" className="tag-pill tag-default" onClick={showTagHandler}>
        {props.tagName}
      </a>
    );
  }

  return <Fragment>{tag}</Fragment>;
};
export default TagItem;
