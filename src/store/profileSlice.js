import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  profile: {},
  isLoading: true,
  isFollowLoading: true,
  error: null,
  followError: "",
};

const profileSlice = createSlice({
  name: "profile",
  initialState,
  reducers: {
    updateError(state, action) {
      state.error = action.payload;
    },
    updateFollowError(state, action) {
      state.followError = action.payload;
    },
    updateIsLoading(state, action) {
      state.isLoading = action.payload;
    },
    updateIsFollowLoading(state, action) {
      state.isFollowLoading = action.payload;
    },
    addProfile(state, action) {
      state.profile = action.payload;
    },
    removeProfile(state) {
      state.profile = {};
    },
    resetProfile(state) {
      state.profile = {};
      state.isLoading = true;
      state.isFollowLoading = true;
      state.error = null;
      state.followError = null;
    },
  },
});

export const profileActions = profileSlice.actions;
export default profileSlice;
