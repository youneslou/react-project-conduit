import { articlesActions } from "./articlesSlice";

const fitchArticles = (requestParam) => {
  return async (dispatch) => {
    const sendRequest = async () => {
      dispatch(articlesActions.updateIsLoading(true));

      const response = await fetch(
        `https://conduit.productionready.io/api/articles${requestParam}`
      );
      if (!response.ok) {
        throw new Error("echec");
      }
      const data = await response.json();
      return data;
    };
    try {
      const data = await sendRequest();

      dispatch(articlesActions.updateArticles(data.articles));
      dispatch(articlesActions.updateArticlesCount(data.articlesCount));
      dispatch(articlesActions.updateIsLoading(false));
    } catch (error) {
      dispatch(articlesActions.updateError(error.message || "error"));
    }
  };
};

export const fitchMyFeed = (requestParam) => {
  return async (dispatch) => {
    const sendRequest = async () => {
      dispatch(articlesActions.updateIsLoading(true));
      const response = await fetch(
        `https://conduit.productionready.io/api/articles/feed${requestParam}`,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            authorization: `Token ${localStorage.getItem("jwtToken")}`,
          },
        }
      );
      if (!response.ok) {
        throw new Error("echec");
      }
      const data = await response.json();
      return data;
    };
    try {
      const data = await sendRequest();
      dispatch(articlesActions.updateArticles(data.articles));
      dispatch(articlesActions.updateArticlesCount(data.articlesCount));
      dispatch(articlesActions.updateIsLoading(false));
    } catch (error) {
      dispatch(articlesActions.updateError(error.message || "error"));
    }
  };
};

export const fitchFavArticlesWithParames = (requestParam) => {
  return async (dispatch) => {
    const sendRequest = async () => {
      dispatch(articlesActions.updateIsLoading(true));
      dispatch(articlesActions.updateError(null));

      const response = await fetch(
        `https://conduit.productionready.io/api/articles${requestParam}`,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            authorization: `Token ${localStorage.getItem("jwtToken")}`,
          },
        }
      );
      if (!response.ok) {
        throw new Error("echec");
      }
      const data = await response.json();
      return data;
    };
    try {
      const data = await sendRequest();
      dispatch(articlesActions.updateArticles(data.articles));
      dispatch(articlesActions.updateArticlesCount(data.articlesCount));
      dispatch(articlesActions.updateIsLoading(false));
    } catch (error) {
      dispatch(articlesActions.updateError(error.message || "error"));
    }
  };
};

export default fitchArticles;
