import { NavLink, Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import fitchProfile from "./../../store/profileActions";
import fitchFavArticlesWithParames from "./../../store/articlesActions";
import { articlesActions } from "./../../store/articlesSlice";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEdit,
  faCog,
  faHome,
  faSignInAlt,
  faUserPlus,
} from "@fortawesome/free-solid-svg-icons";

function Header(props) {
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);
  const user = useSelector((state) => state.auth.user);
  const dispatch = useDispatch();
  console.log(user, isAuthenticated);

  const showProfileHandler = (e) => {
    if (isAuthenticated) {
      dispatch(fitchProfile(user.username));
      dispatch(
        fitchFavArticlesWithParames(`?author=${user.username}&limit=5&offset=0`)
      );
      dispatch(articlesActions.updateIsMyArticles(true));
    }
  };

  return (
    <nav className="navbar navbar-light">
      <div className="container">
        <Link className="navbar-brand" to="/">
          conduit
        </Link>
        <ul className="nav navbar-nav pull-xs-right">
          <li className="nav-item">
            {/* <!-- Add "active" className when you're on that page" --> */}
            <NavLink activeClassName={"active"} className="nav-link" to="/home">
              <FontAwesomeIcon icon={faHome} /> Home
            </NavLink>
          </li>
          {isAuthenticated && (
            <li className="nav-item">
              <NavLink
                activeClassName={"active"}
                className="nav-link"
                to="/editor/false"
              >
                <FontAwesomeIcon icon={faEdit} /> New Post
                {/* <i className="ion-compose"></i>&nbsp; */}
              </NavLink>
            </li>
          )}
          {isAuthenticated && (
            <li className="nav-item">
              <NavLink
                activeClassName={"active"}
                className="nav-link"
                to="/settings"
              >
                <FontAwesomeIcon icon={faCog} /> Settings
                {/* <i className="ion-gear-a"></i>&nbsp;Settings */}
              </NavLink>
            </li>
          )}
          {isAuthenticated && (
            <li className="nav-item">
              <NavLink
                activeClassName={"active"}
                className="nav-link"
                id="nav-link__profile"
                to={`/profile/${user.username}`}
                onClick={showProfileHandler}
              >
                <img src={user.image} className="profile-image" />{" "}
                {user.username}
              </NavLink>
            </li>
          )}

          {!isAuthenticated && (
            <li className="nav-item">
              <NavLink
                activeClassName={"active"}
                className="nav-link"
                to="/register"
              >
                <FontAwesomeIcon icon={faUserPlus} /> Sign up
              </NavLink>
            </li>
          )}
          {!isAuthenticated && (
            <li className="nav-item">
              <NavLink
                activeClassName={"active"}
                className="nav-link "
                id="nav-link__login"
                to="/login"
              >
                <FontAwesomeIcon icon={faSignInAlt} /> Sign in
              </NavLink>
            </li>
          )}
        </ul>
      </div>
    </nav>
  );
}

export default Header;
