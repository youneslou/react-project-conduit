import { updatecommentsActions } from "./../../../store/commentsSlice";
import { removeComment } from "./../../../store/commentsActions";
import { useDispatch, useSelector } from "react-redux";
import fitchProfile from "./../../../store/profileActions";
import fitchFavArticlesWithParames from "../../../store/articlesActions";
import { articlesActions } from "./../../../store/articlesSlice";
import { useHistory } from "react-router-dom";

const Comment = (props) => {
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);
  const isCommentLoading = useSelector(
    (state) => state.comments.isCommentLoading
  );
  const user = useSelector((state) => state.auth.user);
  const dispatch = useDispatch();
  const nav = useHistory();

  const removeCommentHandler = (e) => {
    e.preventDefault();
    dispatch(removeComment(props.slug, props.id));
    if (!isCommentLoading)
      dispatch(updatecommentsActions.removeComment(props.id));
    console.log(props.id);
  };
  const showProfileHandler = (e) => {
    e.preventDefault();
    dispatch(fitchProfile(props.author.username));
    dispatch(
      fitchFavArticlesWithParames(
        `?author=${props.author.username}&limit=5&offset=0`
      )
    );
    dispatch(articlesActions.updateIsMyArticles(true));
    nav.push(`/profile/${props.author.username}`);
  };
  return (
    <div className="card">
      <div className="card-block">
        <p className="card-text">{props.body}</p>
      </div>
      <div className="card-footer">
        <a href="" className="comment-author" onClick={showProfileHandler}>
          <img src={props.author.image} className="comment-author-img" />
        </a>
        &nbsp;
        <a href="" className="comment-author" onClick={showProfileHandler}>
          {props.author.username}
        </a>
        <span className="date-posted">
          {Intl.DateTimeFormat("en-US").format(new Date(props.createdAt))}
        </span>
        {user.username === props.author.username && isAuthenticated && (
          <button
            className="btn btn-sm btn-danger btn-remove-comment"
            onClick={removeCommentHandler}
          >
            remove comment
          </button>
        )}
      </div>
    </div>
  );
};

export default Comment;
