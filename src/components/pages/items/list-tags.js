import TagItem from "./item-tag";

function TagList(props) {
  return (
    <ul className="tag-list">
      {props.isTagsLoading ? (
        <p>Loading...</p>
      ) : (
        props.tagList.map((item) => {
          return (
            <TagItem
              isArticleItem={props.isArticleItem}
              onClick={props.onClick}
              key={item}
              tagName={item}
            />
          );
        })
      )}
    </ul>
  );
}
export default TagList;
