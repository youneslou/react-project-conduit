import Header from "./components/layout/header";
import LogReg from "./components/pages/LogReg";
import { Route, Switch, Redirect } from "react-router-dom";
import Footer from "./components/layout/footer";
import Home from "./components/pages/home";
import Article from "./components/pages/article-page";
import React, { useEffect, Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import initFetchUser from "./store/authActions";
import CreateEdit from "./components/pages/createEdit";
import Settings from "./components/pages/settings";
import Profile from "./components/pages/profile";
import PageNotFound from "./components/pages/Page-not-found";

function App() {
  const dispatch = useDispatch();
  const isRander = useSelector((state) => state.auth.isRander);

  useEffect(() => {
    dispatch(initFetchUser());
  }, [dispatch]);

  return (
    <Fragment>
      {isRander && (
        <div>
          <Header />
          <Switch>
            <Route path="/" exact>
              <Redirect to="/home" />
            </Route>
            <Route path="/home">
              <Home />
            </Route>
            <Route path="/login">
              <LogReg />
            </Route>
            <Route path="/register">
              <LogReg />
            </Route>
            <Route path={"/profile/:username"}>
              <Profile />
            </Route>
            <Route path={"/settings/"}>
              <Settings />
            </Route>
            <Route path={"/article/:slug"}>
              <Article />
            </Route>
            <Route path={"/editor/:edit"}>
              <CreateEdit />
            </Route>
            <Route path={`*`}>
              <PageNotFound />
            </Route>
          </Switch>

          <Footer />
        </div>
      )}
    </Fragment>
  );
}

export default App;
