// import authActions from "../store/authSlise";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import { useHistory } from "react-router";
import useHttp from "./../../hooks/use-http";
import useInput from "./../../hooks/use-input";

import { authActions } from "./../../store/authSlice";
import { tagListActions } from "./../../store/tagsSlice";
import { articlesActions } from "./../../store/articlesSlice";
import { articleActions } from "./../../store/articleSlice";
import { updatecommentsActions } from "./../../store/commentsSlice";
import { profileActions } from "./../../store/profileSlice";

const Settings = () => {
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);
  const user = useSelector((state) => state.auth.user);
  const { sendRequest, isLoading, error } = useHttp();
  const dispatch = useDispatch();

  const nav = useHistory();

  const {
    value: enteredImage,
    isValid: imageIsValid,
    hasError: imageHasError,
    setValue: setImage,
    valueChangeHandler: imageChangeHandler,
    valueBlurHandler: imageBlurHandler,
    reset: resetImage,
  } = useInput((value) => value !== "");
  const {
    value: enteredName,
    isValid: nameIsValid,
    hasError: nameHasError,
    setValue: setName,
    valueChangeHandler: nameChangeHandler,
    valueBlurHandler: nameBlurHandler,
    reset: resetName,
  } = useInput((value) => value !== "");
  const {
    value: enteredBio,
    isValid: bioIsValid,
    hasError: bioHasError,
    setValue: setBio,
    valueChangeHandler: bioChangeHandler,
    valueBlurHandler: bioBlurHandler,
    reset: resetBio,
  } = useInput((value) => value !== "");
  const {
    value: enteredEmail,
    isValid: emailIsValid,
    hasError: emailHasError,
    setValue: setEmail,
    valueChangeHandler: emailChangeHandler,
    valueBlurHandler: emailBlurHandler,
    reset: resetEmail,
  } = useInput((value) => value !== "");
  const {
    value: enteredPassword,
    isValid: passwordIsValid,
    hasError: passwordHasError,
    setValue: setPassword,
    valueChangeHandler: passwordChangeHandler,
    valueBlurHandler: passwordBlurHandler,
    reset: resetPassword,
  } = useInput((value) => value !== "");

  let formIsValid = false;
  if (
    passwordIsValid &&
    emailIsValid &&
    bioIsValid &&
    nameIsValid &&
    imageIsValid
  ) {
    formIsValid = true;
  }

  useEffect(() => {
    console.log("********init edit true*******");
    if (isAuthenticated) {
      setImage(user.image);
      setName(user.username);
      setBio(user.bio);
      setEmail(user.email);
    }
  }, [user, isAuthenticated]);

  const formHandler = (e) => {
    e.preventDefault();

    const applyData = (data) => {
      dispatch(authActions.updateUser(data.user));
      nav.push("/");
    };
    sendRequest(
      {
        url: "https://conduit.productionready.io/api/user",
        method: "PUT",
        body: {
          user: {
            email: enteredEmail,
            bio: enteredBio,
            image: enteredImage,
          },
        },
        authorization: `Token ${localStorage.getItem("jwtToken")}`,
      },
      applyData
    );
  };

  const logoutHandler = (e) => {
    nav.replace("/");
    dispatch(authActions.logout());
    // dispatch(tagListActions.resetTag());
    dispatch(articlesActions.resetArticles());
    // dispatch(articleActions.resetArticles());
    // dispatch(updatecommentsActions.resetComments());
    // dispatch(profileActions.resetProfile());
    localStorage.removeItem("jwtToken");
  };

  return (
    <div className="settings-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-6 offset-md-3 col-xs-12">
            <h1 className="text-xs-center">Your Settings</h1>

            <form onSubmit={formHandler}>
              <fieldset>
                <fieldset className="form-group">
                  <input
                    className="form-control"
                    type="text"
                    placeholder="URL of profile picture"
                    value={enteredImage}
                    onChange={imageChangeHandler}
                    onBlur={imageBlurHandler}
                  />
                </fieldset>{" "}
                <fieldset className="form-group">
                  <input
                    className="form-control form-control-lg"
                    type="text"
                    placeholder="Your Name"
                    value={enteredName}
                    onChange={nameChangeHandler}
                    onBlur={nameBlurHandler}
                  />
                </fieldset>
                <fieldset className="form-group">
                  <textarea
                    className="form-control form-control-lg"
                    rows="8"
                    placeholder="Short bio about you"
                    value={enteredBio}
                    onChange={bioChangeHandler}
                    onBlur={bioBlurHandler}
                  ></textarea>
                </fieldset>
                <fieldset className="form-group">
                  <input
                    className="form-control form-control-lg"
                    type="text"
                    placeholder="Email"
                    value={enteredEmail}
                    onChange={emailChangeHandler}
                    onBlur={emailBlurHandler}
                  />
                </fieldset>
                <fieldset className="form-group">
                  <input
                    className="form-control form-control-lg"
                    type="password"
                    placeholder="Password"
                    value={enteredPassword}
                    onChange={passwordChangeHandler}
                    onBlur={passwordBlurHandler}
                  />
                </fieldset>
                <button
                  disabled={!formIsValid}
                  className="btn btn-lg btn-primary pull-xs-right"
                >
                  Update Settings
                </button>
              </fieldset>
            </form>
          </div>
        </div>
        <div className="row-logout">
          <button
            className="btn-logout btn-outline-danger"
            onClick={logoutHandler}
          >
            Logout
          </button>
        </div>
      </div>
    </div>
  );
};

export default Settings;
