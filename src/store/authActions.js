import { authActions } from "./authSlice";

const initFetchUser = () => {
  return async (dispatch) => {
    if (localStorage.getItem("jwtToken")) {
      const sendRequest = async () => {
        dispatch(authActions.updateIsLoading(true));
        dispatch(authActions.updateError(null));

        const response = await fetch(
          "https://conduit.productionready.io/api/user",
          {
            method: "GET",
            headers: {
              Accept: "application/json",
              authorization: `Token ${localStorage.getItem("jwtToken")}`,
            },
          }
        );
        if (!response.ok) {
          throw new Error("echec");
        }
        const data = await response.json();
        return data;
      };
      try {
        const data = await sendRequest();
        dispatch(authActions.login(data.user));
        dispatch(authActions.updateIsLoading(false));
      } catch (error) {
        dispatch(authActions.updateError(error.message || "error"));
      }
    } else {
      dispatch(authActions.logout());
    }
  };
};

export const fetchUser = (email, password) => {
  return async (dispatch) => {
    const sendRequest = async () => {
      dispatch(authActions.updateIsLoading(true));
      dispatch(authActions.updateError(null));

      const response = await fetch(
        "https://conduit.productionready.io/api/users/login",
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            user: { email, password },
          }),
        }
      );
      if (!response.ok) {
        throw new Error("echec");
      }
      const data = await response.json();
      return data;
    };
    try {
      const data = await sendRequest();
      console.log(data);
      dispatch(authActions.login(data.user));
      dispatch(authActions.updateIsLoading(false));
      localStorage.setItem("jwtToken", data.user.token);
    } catch (error) {
      dispatch(authActions.updateError(error.message || "error"));
      console.log(error);
    }
  };
};

export const updateUser = (body) => {
  return async (dispatch) => {
    if (localStorage.getItem("jwtToken")) {
      const sendRequest = async () => {
        dispatch(authActions.updateIsLoading(true));
        dispatch(authActions.updateError(null));

        const response = await fetch(
          "https://conduit.productionready.io/api/user",
          {
            method: "PUT",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              authorization: `Token ${localStorage.getItem("jwtToken")}`,
            },
            body: JSON.stringify({
              user: body,
            }),
          }
        );
        if (!response.ok) {
          throw new Error("echec");
        }
        const data = await response.json();
        return data;
      };
      try {
        const data = await sendRequest();
        dispatch(authActions.login(data.user));
        dispatch(authActions.updateIsLoading(false));
      } catch (error) {
        dispatch(authActions.updateError(error.message || "error"));
      }
    } else {
      dispatch(authActions.logout());
    }
  };
};
export default initFetchUser;
