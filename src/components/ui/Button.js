import { Fragment } from "react";

function Button(props) {
  return (
    <Fragment>
      <button
        className="btn btn-lg btn-primary pull-xs-right"
        type={props.type}
        disabled={props.disabled}
      >
        {props.content}
      </button>
    </Fragment>
  );
}
export default Button;
