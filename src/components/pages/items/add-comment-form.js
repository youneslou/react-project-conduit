import { useDispatch } from "react-redux";
import { useRef } from "react";
import { addComment } from "./../../../store/commentsActions";

const AddCommentForm = (props) => {
  const dispatch = useDispatch();
  const commentRef = useRef();

  const addCommentHandler = (e) => {
    e.preventDefault();
    const comment = commentRef.current.value;
    dispatch(
      addComment(props.slug, {
        body: comment,
      })
    );
  };
  return (
    <form className="card comment-form" onSubmit={addCommentHandler}>
      <div className="card-block">
        <textarea
          className="form-control"
          placeholder="Write a comment..."
          rows="3"
          ref={commentRef}
        ></textarea>
      </div>
      <div className="card-footer">
        <img src={props.userImage} className="comment-author-img" />
        <button className="btn btn-sm btn-primary" type="submit">
          Post Comment
        </button>
      </div>
    </form>
  );
};
export default AddCommentForm;
