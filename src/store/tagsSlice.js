import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  tagList: [],
  selectedTag: "",
  isTag: false,
  isLoading: true,
  error: null,
};

const tagList = createSlice({
  name: "tags",
  initialState,
  reducers: {
    updateError(state, action) {
      state.error = action.payload;
    },
    updateSelectedTag(state, action) {
      state.selectedTag = action.payload;
    },
    updateIsTag(state, action) {
      state.isTag = action.payload;
    },
    updateTagList(state, action) {
      state.tagList = action.payload;
    },
    updateIsLoading(state, action) {
      state.isLoading = action.payload;
    },
    resetTag(state) {
      state.tagList = [];
      state.selectedTag = "";
      state.isTag = false;
      state.isLoading = true;
      state.error = null;
    },
  },
});

export const tagListActions = tagList.actions;
export default tagList;
