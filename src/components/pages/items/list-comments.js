import { Fragment } from "react";
import Comment from "./item-comment";

const ListComments = (props) => {
  console.log(props.comments);
  return (
    <Fragment>
      {props.comments.map((item) => {
        return (
          <Comment
            key={item.id}
            slug={props.slug}
            id={item.id}
            createdAt={item.createdAt}
            updatedAt={item.updatedAt}
            body={item.body}
            author={item.author}
          />
        );
      })}
    </Fragment>
  );
};

export default ListComments;
