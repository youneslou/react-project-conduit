import { articleActions } from "./articleSlice";

const fitchArticle = (requestParam) => {
  return async (dispatch) => {
    const sendRequest = async () => {
      dispatch(articleActions.updateIsLoading(true));
      dispatch(articleActions.updateError(null));

      const response = await fetch(
        `https://conduit.productionready.io/api/articles${requestParam}`
      );
      if (!response.ok) {
        throw new Error("echec");
      }
      const data = await response.json();
      return data;
    };
    try {
      const data = await sendRequest();

      dispatch(articleActions.updateArticle(data.article));
      dispatch(articleActions.updateIsLoading(false));
    } catch (error) {
      dispatch(articleActions.updateError(error.message || "error"));
    }
  };
};
export default fitchArticle;
