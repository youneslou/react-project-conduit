import { useSelector, useDispatch } from "react-redux";
import React, { Fragment } from "react";
import { useHistory } from "react-router";
import ListComments from "./items/list-comments";
import AddCommentForm from "./items/add-comment-form";
import useHttp from "./../../hooks/use-http";
import { followUnfollowUser } from "./../../store/profileActions";
import fitchProfile from "./../../store/profileActions";
import fitchFavArticlesWithParames from "./../../store/articlesActions";
import { articlesActions } from "./../../store/articlesSlice";
import { articleActions } from "./../../store/articleSlice";

function Article() {
  const user = useSelector((state) => state.auth.user);
  const profile = useSelector((state) => state.profile.profile);
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);
  const article = useSelector((state) => state.article.article);
  const isLoading = useSelector((state) => state.article.isLoading);
  const comments = useSelector((state) => state.comments.comments);
  const isCommentsLoading = useSelector((state) => state.comments.isLoading);
  const nav = useHistory();
  const dispatch = useDispatch();
  const {
    sendRequest: sendFavRequest,
    isLoading: isFavLoading,
    error: favError,
  } = useHttp();
  const {
    sendRequest: sendDeleteRequest,
    isLoading: isDeleteLoading,
    error: deleteError,
  } = useHttp();

  const editArticleHandler = (e) => {
    nav.push(`/editor/true`);
  };

  const deleteArticleHandler = (e) => {
    const applyData = (data) => {
      if (isAuthenticated) {
        dispatch(fitchProfile(user.username));
        dispatch(
          fitchFavArticlesWithParames(
            `?author=${user.username}&limit=5&offset=0`
          )
        );
        dispatch(articlesActions.updateIsMyArticles(true));
      }
      nav.replace(`/profile/${user.username}`);
    };
    sendDeleteRequest(
      {
        url: `https://conduit.productionready.io/api/articles/${article.slug}`,
        method: "DELETE",
        authorization: `Token ${localStorage.getItem("jwtToken")}`,
      },
      applyData
    );
  };

  const favoriteHandler = (e) => {
    if (isAuthenticated) {
      let method = "";

      if (article.favorited) {
        method = "DELETE";
      } else {
        method = "POST";
      }

      const applyData = (data) => {
        console.log(data);
        dispatch(articleActions.updateArticle(data.article));
      };
      sendFavRequest(
        {
          url: `https://conduit.productionready.io/api/articles/${article.slug}/favorite`,
          method,
          authorization: `Token ${localStorage.getItem("jwtToken")}`,
        },
        applyData
      );
    } else {
      nav.push("/login");
    }
  };

  const followUnfollowHandler = (e) => {
    if (profile.following) {
      dispatch(followUnfollowUser("DELETE", profile.username));
    } else {
      dispatch(followUnfollowUser("POST", profile.username));
    }
  };

  const showProfileHandler = (e) => {
    e.preventDefault();
    dispatch(fitchProfile(article.author.username));
    dispatch(
      fitchFavArticlesWithParames(
        `?author=${article.author.username}&limit=5&offset=0`
      )
    );
    dispatch(articlesActions.updateIsMyArticles(true));
    nav.push(`/profile/${article.author.username}`);
  };

  return (
    <React.Fragment>
      {!isLoading && (
        <div className="article-page">
          <div className="banner">
            <div className="container">
              <h1>{article.title}</h1>

              <div className="article-meta">
                <a href="" onClick={showProfileHandler}>
                  <img src={`${article.author.image}`} />
                </a>
                <div className="info">
                  <a href="" className="author" onClick={showProfileHandler}>
                    {article.author.username}
                  </a>
                  <span className="date">
                    {Intl.DateTimeFormat("en-US").format(
                      new Date(article.createdAt)
                    )}
                  </span>
                </div>
                {isAuthenticated &&
                article.author.username === user.username ? (
                  <Fragment>
                    <button
                      className="btn btn-sm btn-outline-secondary"
                      onClick={editArticleHandler}
                    >
                      <i className="ion-edit"></i>
                      &nbsp; Edit Article
                    </button>
                    &nbsp;&nbsp;
                    <button
                      className="btn btn-outline-danger btn-sm"
                      onClick={deleteArticleHandler}
                    >
                      <i className="ion-trash-a"></i>
                      &nbsp; Delete Article
                    </button>
                  </Fragment>
                ) : (
                  article.author.username !== user.username && (
                    <Fragment>
                      <button
                        className="btn btn-sm btn-outline-secondary"
                        onClick={followUnfollowHandler}
                      >
                        <i className="ion-plus-round"></i>
                        &nbsp; {`${
                          profile.following ? "UnFollow" : "Follow"
                        }`}{" "}
                        {article.author.username}{" "}
                        <span className="counter">
                          ({article.favoritesCount})
                        </span>
                      </button>
                      &nbsp;&nbsp;
                      <button
                        className="btn btn-sm btn-outline-primary"
                        onClick={favoriteHandler}
                      >
                        <i className="ion-heart"></i>
                        &nbsp;{" "}
                        {`${
                          article.favorited ? "Unfavorite" : "Favorite"
                        } Post`}
                        <span className="counter">
                          ({article.favoritesCount})
                        </span>
                      </button>
                    </Fragment>
                  )
                )}
              </div>
            </div>
          </div>

          <div className="container page">
            <div className="row article-content">
              <div className="col-md-12">
                <p>{article.description}</p>
                <h2 id="introducing-ionic">{article.title}</h2>
                <p>{article.body}</p>
              </div>
            </div>

            <hr />

            <div className="article-actions">
              <div className="article-meta">
                <a href="profile.html" onClick={showProfileHandler}>
                  <img src={article.author.image} />
                </a>
                <div className="info">
                  <a href="" className="author" onClick={showProfileHandler}>
                    {article.author.username}
                  </a>
                  <span className="date">
                    {Intl.DateTimeFormat("en-US").format(
                      new Date(article.createdAt)
                    )}
                  </span>
                </div>
                {isAuthenticated &&
                article.author.username === user.username ? (
                  <Fragment>
                    <button
                      className="btn btn-sm btn-outline-secondary"
                      onClick={editArticleHandler}
                    >
                      <i className="ion-edit"></i>
                      &nbsp; Edit Article
                    </button>
                    &nbsp;&nbsp;
                    <button
                      className="btn btn-outline-danger btn-sm"
                      onClick={deleteArticleHandler}
                    >
                      <i className="ion-trash-a"></i>
                      &nbsp; Delete Article
                    </button>
                  </Fragment>
                ) : (
                  <Fragment>
                    <button
                      className="btn btn-sm btn-outline-secondary"
                      onClick={followUnfollowHandler}
                    >
                      <i className="ion-plus-round"></i>
                      &nbsp; {`${
                        profile.following ? "UnFollow" : "Follow"
                      }`}{" "}
                      {profile.username} <span className="counter">(29)</span>
                    </button>
                    &nbsp;&nbsp;
                    <button
                      className="btn btn-sm btn-outline-primary"
                      onClick={favoriteHandler}
                    >
                      <i className="ion-heart"></i>
                      &nbsp;{" "}
                      {`${
                        article.favorited ? "Unfavorite" : "Favorite"
                      } Post`}{" "}
                      <span className="counter">
                        ({article.favoritesCount})
                      </span>
                    </button>
                  </Fragment>
                )}
              </div>
            </div>

            <div className="row">
              <div className="col-xs-12 col-md-8 offset-md-2">
                {isAuthenticated && (
                  <AddCommentForm slug={article.slug} userImage={user.image} />
                )}

                {/* <ListComments slug={article.slug} comments={comments} /> */}

                {isCommentsLoading ? (
                  <span>Loading...</span>
                ) : (
                  <ListComments slug={article.slug} comments={comments} />
                )}
              </div>
            </div>
          </div>
        </div>
      )}
    </React.Fragment>
  );
}
export default Article;
